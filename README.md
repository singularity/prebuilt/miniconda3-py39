# miniconda3 python 3.9 Singularity container
Based on debian 9.8 slim<br>

Singularity container based on the recipe: Singularity.miniconda3_py39_4.11.0.def

Package installation using Miniconda3 python3 3.9 version 4.11.0<br>

Used as base to build containers<br>
- local image, header:<br>
	boostrap:localimage <br>
	from:miniconda3_py39_4.11.0.sif <br>
- Or via oras, header: <br>
        boostrap:oras <br>
        from:oras://registry.forgemia.inra.fr/singularity/prebuilt/miniconda3_py39/miniconda3_py39:latest <br>

Can be used in a Multi-Stage build (pack is installed) <br>


usage:<br>
     miniconda3_py39_4.11.0.sif --help


Image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>
```bash
singularity pull miniconda3_py39_4.11.0.sif oras://registry.forgemia.inra.fr/singularity/prebuilt/miniconda3_py39/miniconda3_py39:latest
```

For local build:
```bash
sudo singularity build miniconda3_py39_4.11.0.sif Singularity.miniconda3_py39_4.11.0.def
```

contact: jacques.lagnel@inrae.fr

